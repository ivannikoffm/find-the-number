// Global variables
let counter = 1;
let timer = null;

document.querySelector('#startbtn').addEventListener('click', (event) => {
    startNewGame();
    event.currentTarget.style.display = 'none';
});

// Generates the table and fill it. Initiates timer and game logic
function startNewGame() {
    counter = 1;
    let numbers = generateRandomSequence(1, 25);
    let table = createTable(numbers);
    document.querySelector('main').appendChild(table);
    document.querySelector('#descr').innerHTML = 'Game started'; 

    if (document.querySelector('#resetbtn') == undefined) document.querySelector('main').appendChild(GenerateResetBtn());
    timer = setInterval(countDown, 1000);
}

// Generates a numeric array in a specified interval
function generateRandomSequence(startNum, lastNum) {
    if (startNum <= lastNum) Error('The maximum and minimum values are not comparable'); // Check numbers

    // Two arrays. One will be ordered and other one random nums on random indexes
    let orderedNumArray = [];
    let mixedNumArray = [];

    // Filling the ordered array with consecutive elements
    for (let i = startNum; i <= lastNum; i++)
        orderedNumArray.push(i);

    // Generate a random index and insert it into a new array (mixedNumArray). Then delete this index from the ordered array
    while (orderedNumArray.length > 0) {
        let randomIndex = generateRandomNumber(0, orderedNumArray.length - 1);
        mixedNumArray.push(orderedNumArray[randomIndex]);
        orderedNumArray.splice(randomIndex, 1);
    }
    return mixedNumArray; // Mixed array is done
}

function generateRandomNumber(min, max) {
    return Math.floor(Math.random() * (max + 1 - min)) + min;
}

// Creates a table and fills it with values from the provided array in a loop
function createTable(contentArray) {
    let table = document.createElement('table');
    for (let i = 0; i < 5; i++) {
        let tr = document.createElement('tr');
        for (let a = 0; a < 5; a++) {
            let td = document.createElement('td');
            td.innerHTML = contentArray[(a + (i * 5))];
            td = generateRandomStyle(td);
            td.addEventListener('click', tdOnClick);
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
    return table;
}

// Defines the logic when clicking on the correct cell - it is marked with a color and the counter increases to the next value
function tdOnClick(event) {
    if (counter == +event.currentTarget.innerHTML) {
        event.currentTarget.style.backgroundColor = 'red';
        counter++;
        if (counter == 26) {
            alert('You Won!');
            document.querySelector('#descr').innerHTML = 'You WON'; 
            clearTimeout(timer);
        }
    }
}

// Generates random styles for table cells
function generateRandomStyle(node) {
    node.style.fontSize = generateRandomNumber(16, 30) + 'px';
    node.style.color = `rgb(${generateRandomNumber(0, 255)},${generateRandomNumber(0, 255)},${generateRandomNumber(0, 255)})`;
    return node;
}

// Timer countdown logic. Check for 0 seconds and lose
function countDown() {
    let time = +document.querySelector('#timer span').innerHTML;
    if (time > 0) 
        document.querySelector('#timer span').innerHTML = --time; 
    else {
        clearTimeout(timer);
        document.querySelector('#descr').innerHTML = 'You failed'; 
        document.querySelector('table').remove();
    }
}

// Reset button - reset all state and initiate the game
function GenerateResetBtn() {
    let button = document.createElement('input');
    button.type = 'button';
    button.value = 'Reset';
    button.id = 'resetbtn';
    button.addEventListener('click', () => {
        clearTimeout(timer);
        if (document.querySelector('table') != null) document.querySelector('table').remove();
        document.querySelector('#timer span').innerHTML = 75; 
        startNewGame();
    });
    return button;
}